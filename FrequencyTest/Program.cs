﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace FrequencyTest
{
    class Program
    {
        static void Main(string[] args)
        {
            string fileName = args[0];
            int keySize = int.Parse(args[1]);
            int column = int.Parse(args[2]);

            if (column >= keySize) return;

            StreamReader sr = new StreamReader(File.Open(fileName, FileMode.Open));
            string cyphertext = sr.ReadToEnd();
            sr.Close();

            int[] f = new int[26];
            for (int i = column; i < cyphertext.Length; i += keySize)
            {
                char lowerChar = Char.ToLower(cyphertext[(int)i]);
                int charIndex = ((int)lowerChar) - 97;
                f[charIndex]++;
            }

            for (int c = 0; c < 26 ; c++)
            {
                if(f[c] != 0)
                {
                    char charValue = (char)(c + 97);
                    Console.WriteLine(charValue + " = " + f[c]);
                }
            }

        }
    }
}
