﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace VigenereDecrypt
{
    class Program
    {
        static void Main(string[] args)
        {
            string fileName = args[0];
            string key = args[1];

            //read file
            StreamReader sr = new StreamReader(File.Open(fileName, FileMode.Open));
            string cyphertext = sr.ReadToEnd();
            sr.Close();

            StringBuilder sb = new StringBuilder(cyphertext.Length);

            for(int i = 0; i <  cyphertext.Length; i++)
            {
                char cypheredLowerChar = Char.ToLower(cyphertext[(int)i]);
                int cypheredCharIndex = ((int)cypheredLowerChar) - 97;

                char keyLowerChar = Char.ToLower(key[(int)i % key.Length]);
                int keyCharIndex = ((int)keyLowerChar) - 97;

                int plainCharIndex = (cypheredCharIndex + (26 - keyCharIndex)) % 26;
                sb.Append((char)(plainCharIndex + 97));
                
            }
            Console.WriteLine(sb.ToString());
        }
    }
}
