﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace FriedmanTest
{
    class Program
    {
        static void Main(string[] args)
        {
            //read file
            StreamReader sr = new StreamReader(File.Open(args[0], FileMode.Open));
            string cyphertext = sr.ReadToEnd();
            sr.Close();

            uint keySize = 0;
            bool keySizeFound = false;
            while (!keySizeFound)
            {
                keySize++;
                Console.WriteLine("Trying with KeySize = {0}", keySize); 
                float[] ic = CoincidenceIndexes(cyphertext, keySize);
                for (int i = 0; i < ic.Length; i++)
                {
                    if (ic[i] < 0.063)
                    {
                        keySizeFound = false;
                        break;
                    }
                    else
                    {
                        keySizeFound = true;
                    }
                }   
            }
            Console.WriteLine("KeySize = {0}", keySize);    
        }

        static float[] CoincidenceIndexes(string cyphertext, uint keySize)
        {
            if (keySize == 0) return null;

            float[] coincidenceIndexes = new float[keySize];

            //calculate f[i]

            for(uint k = 0; k < keySize; k++)
            {
                int[] f = new int[26];
                uint n = 0;
                for (uint i = k; i < cyphertext.Length; i += keySize)
                {
                    char lowerChar = Char.ToLower(cyphertext[(int)i]);
                    int charIndex = ((int)lowerChar) - 97;
                    f[charIndex]++;
                    n++;
                }
                coincidenceIndexes[k] = CoincidenceIndex(f, n);

            }
            return coincidenceIndexes;
            
        }

        static float CoincidenceIndex(int[] f, uint n)
        {
            float sum = 0;
            for (int i = 0; i < 26; i++)
            {
                sum += (f[i] * (f[i] - 1));
            }
            float total = sum / (n * (n - 1));
            return total;
        }
    }
}
