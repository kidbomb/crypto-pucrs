﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace CaesarBruteForce
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamReader sr = new StreamReader(File.Open(args[0], FileMode.Open));
            string cyphertext = sr.ReadToEnd();
            sr.Close();
            for (int key = 0; key < 26; key++)
            {
                Console.WriteLine("-------KEY = {0}------",  key);
                Console.WriteLine(Caesar(cyphertext, key));
                Console.WriteLine("---------------");
            }
        }
        static string Caesar(string cyphertext, int key)
        {
            StringBuilder sb = new StringBuilder(cyphertext.Length);
            for (int i = 0; i < cyphertext.Length; i++)
            {
                int charValue = ((int) cyphertext[i]) - 97; //start with zero
                int newCharValue = (charValue + key) % 26;
                sb.Append((char)(newCharValue + 97)); 
            }
            return sb.ToString();
        }
    }
}
