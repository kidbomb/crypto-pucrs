﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace VigenereFrequencyTest
{
    class Program
    {
        static void Main(string[] args)
        {
            string fileName = args[0];
            int keySize = int.Parse(args[1]);
            //read file
            StreamReader sr = new StreamReader(File.Open(fileName, FileMode.Open));
            string cyphertext = sr.ReadToEnd();
            sr.Close();

            StringBuilder key = new StringBuilder(keySize);
            for (int column = 0; column < keySize; column++)
            {
                int[] f = new int[26];
                for (int i = column; i < cyphertext.Length; i += keySize)
                {
                    char lowerChar = Char.ToLower(cyphertext[(int)i]);
                    int charIndex = ((int)lowerChar) - 97;
                    f[charIndex]++;
                }

                int greatestFrequency = 0;
                int greatestFrequencyIndex = 0;
                for (int i = 0; i < 26; i++)
                {
                    if (f[i] > greatestFrequency)
                    {
                        greatestFrequency = f[i];
                        greatestFrequencyIndex = i;
                    }
                }

                //the greatesf frequency is probably 'e'
                int keyCharIndex = (greatestFrequencyIndex + (26 - 5) + 1) % 26;

                key.Append((char)(keyCharIndex + 97));
            }

            Console.WriteLine("key = " + key.ToString());

        }
    }
}
